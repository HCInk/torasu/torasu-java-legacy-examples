package de.hcinc.torasu.example;

import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.Renderable.RenderResult;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultFormatSettings;
import de.hcinc.torasu.log.ConsoleLogObserver;
import de.hcinc.torasu.log.LogEntry.LogLevel;
import de.hcinc.torasu.log.LogInstruction;
import de.hcinc.torasu.pipeline.RenderRunner;
import de.hcinc.torasu.std.rnd.RFixedDouble;
import de.hcinc.torasu.std.rnd.RMultiplySimple;
import de.hcinc.torasu.util.RenderUtils;

public class TorasuExample {
	
	public static void main(String[] args) throws InterruptedException {
		RFixedDouble valx = new RFixedDouble(10D);
		RFixedDouble valy = new RFixedDouble(10D);
		
		RFixedDouble vala = new RFixedDouble(10D);
		RFixedDouble valb = new RFixedDouble(10D);

		RMultiplySimple mul1 = new RMultiplySimple(valx, valy);
		RMultiplySimple mul2 = new RMultiplySimple(vala, valb);

		RMultiplySimple mul = new RMultiplySimple(mul1, mul2);
		
		RMultiplySimple mulRes = new RMultiplySimple(mul, new RFixedDouble(0.001D));
		
		//Create a render runner with 20 threads to use
		RenderRunner rr = new RenderRunner(20);
		
		Thread rrThread = new Thread(rr, "RenderRunner");
		rrThread.start();
		
		/*
		Thread.sleep(1000L);
		
		long startTime = System.currentTimeMillis();
		
		Queue<Long> renderIds = new LinkedBlockingQueue<Long>();

		for (int rep = 0; rep < 1000; rep++) {
			for (int i = 0; i < 1000; i++) {
				
				long renderId = rr.enqueueRender(mulRes, new RenderContext(null, null), 
						RenderUtils.simpleSettings("v", "de.hcinc.torasu.numeric", new ResultFormatSettings("java.primitive.double", null, null)), 
						i);
				
				renderIds.add(renderId);
				
			}
			
			while (!renderIds.isEmpty()) {
				long rid = renderIds.poll();
				System.out.println("#" + rep + "~W" + rid);
				rr.fetchRenderResult(rid);
				//System.out.println(RenderResult.getResultDisplay(rr.fetchRenderResult(renderIds.poll())));
			}
		
		}
		
		System.out.println("WORK TIME: " + (System.currentTimeMillis()-startTime));
		*/
		
		Thread.sleep(1000L);
		
		LogInstruction log = new LogInstruction(new ConsoleLogObserver(System.out, ConsoleLogObserver.LOG_ERROR_INFO), LogLevel.Debug, true, true);
		
		RenderResult res = rr.runRender(mulRes, new RenderContext(null, null), 
				RenderUtils.simpleSettings("v", "de.hcinc.torasu.numeric", new ResultFormatSettings("java.primitive.double", null, null)), 
				0L, log);
		
		System.out.println(RenderResult.getResultDisplay(res));
		
		rr.stop();
	}
	
}
