package de.hcinc.torasu.example;

import de.hcinc.torasu.log.ConsoleLogObserver;
import de.hcinc.torasu.log.LogEntry.LogLevel;
import de.hcinc.torasu.log.LogEntry.LogMessage;
import de.hcinc.torasu.log.LogObserver;
import de.hcinc.torasu.log.SubLogObserver;

public class LogExample {
	
	public static void main(String[] args) {
		LogObserver obs = new ConsoleLogObserver(System.out);
		
		obs.log(new LogMessage(LogLevel.Info, "HELLO LOG!"));
		
		SubLogObserver sobs1 = new SubLogObserver(obs, "SUB1");

		sobs1.log(new LogMessage(LogLevel.Info, "This has been opened automatically"));
		sobs1.log(new LogMessage(LogLevel.Debug, "SUB ONE test"));

		{
			SubLogObserver sobs3 = new SubLogObserver(sobs1, "SUB3");

			sobs3.log(new LogMessage(LogLevel.Info, "This has been opened automatically and is inside another Sub-Observer"));
			sobs3.log(new LogMessage(LogLevel.Debug, "SUB THREE test"));
			
			sobs3.close();
		}
		{
			SubLogObserver sobs4 = new SubLogObserver(sobs1, "SUB4");

			sobs4.open();
			
			sobs4.log(new LogMessage(LogLevel.Info, "This has been opened manually and is inside another Sub-Observer"));
			sobs4.log(new LogMessage(LogLevel.Debug, "SUB FOUR test"));
			
			
			{	// Direct logging example
				
				SubLogObserver subs1 = new SubLogObserver(sobs4, "SUBS1", 0);

				SubLogObserver subs2 = new SubLogObserver(subs1, "SUBS2", 0);
				
				SubLogObserver subs3 = new SubLogObserver(subs2, "SUBS3", 0, sobs4, new long[] {0,0});
				
				subs3.log(new LogMessage(LogLevel.Info, "SUBS-3-TEST"));

				subs3.close();

				subs2.close();
				
				subs1.close();
				
			}
			
			sobs4.close();
		}
		
		sobs1.close();
		
		SubLogObserver sobs2 = new SubLogObserver(obs, "SUB2");

		sobs2.open();

		sobs2.log(new LogMessage(LogLevel.Info, "This has been opened manually"));
		sobs2.log(new LogMessage(LogLevel.Debug, "SUB TWO test"));
		
		sobs2.close();
		
	}
	
}
